import ChristmasSong from '../main/ChristmasSong.js'

test('Should generate the first line for the day first', () => {
  const ch = new ChristmasSong()
  const firstLine = ch.generateFirstLineForTheDay(1)

  expect(firstLine).toEqual('On the first day of Christmas')
})

test('Should generate the first line for the day twelfth', () => {
  const ch = new ChristmasSong()
  const firstLine = ch.generateFirstLineForTheDay(12)

  expect(firstLine).toEqual('On the twelfth day of Christmas')
})

test('Should generate the second line', () => {
  const ch = new ChristmasSong()
  const secondLine = ch.generateSecondLine()

  expect(secondLine).toEqual('My true love sent to me:')
})

test('Should generate first stuff line', () => {
  const ch = new ChristmasSong()
  const stuffLine = ch.generateTheStuffLineForTheDay(1)

  expect(stuffLine).toEqual('A partridge in a pear tree.')
})

test('Should generate twelfth stuff line', () => {
  const ch = new ChristmasSong()
  const stuffLine = ch.generateTheStuffLineForTheDay(12)

  expect(stuffLine).toEqual(
    'Twelve drummers drumming\nEleven pipers piping\nTen lords a-leaping\nNine ladies dancing\nEight maids a-milking\nSeven swans a-swimming\nSix geese a-laying\nFive golden rings\nFour calling birds\nThree french hens\nTwo turtle doves and\nA partridge in a pear tree.'
  )
})

test('Should generate the whole song for the first day', () => {
  const ch = new ChristmasSong()
  const wholeSong = ch.generateWholeSongForTheDay(1)

  expect(wholeSong).toEqual(
    'On the first day of Christmas\nMy true love sent to me:\nA partridge in a pear tree.'
  )
})

test('Should generate the whole song for the twelfth day', () => {
  const ch = new ChristmasSong()
  const wholeSong = ch.generateWholeSongForTheDay(12)

  expect(wholeSong).toEqual(
    'On the twelfth day of Christmas\nMy true love sent to me:\nTwelve drummers drumming\nEleven pipers piping\nTen lords a-leaping\nNine ladies dancing\nEight maids a-milking\nSeven swans a-swimming\nSix geese a-laying\nFive golden rings\nFour calling birds\nThree french hens\nTwo turtle doves and\nA partridge in a pear tree.'
  )
})
