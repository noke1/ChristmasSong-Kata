Work plan:
- able to generate the first line for the day 
- able to generate the first line parametrised with day number
- able to generate second line (it is always the same)
- able to generate the first stuff line
- able to generate random stuff line
- able to generate the whole song 

To run the tests:
- `npm i` inside the node folder to install all the depenencies
- `npm test -- christmasSong.test.js` to run the tests file