export default class ChristmasSong {
  constructor() {}

  generateFirstLineForTheDay(dayNumber) {
    const days = [
      'first',
      'second',
      'third',
      'forth',
      'fifth',
      'sixth',
      'seventh',
      'eighth',
      'ninth',
      'tenth',
      'eleventh',
      'twelfth',
    ]
    return `On the ${days[dayNumber - 1]} day of Christmas`
  }

  generateSecondLine() {
    return 'My true love sent to me:'
  }

  generateTheStuffLineForTheDay(dayNumber) {
    const stuffs = [
      'Twelve drummers drumming',
      'Eleven pipers piping',
      'Ten lords a-leaping',
      'Nine ladies dancing',
      'Eight maids a-milking',
      'Seven swans a-swimming',
      'Six geese a-laying',
      'Five golden rings',
      'Four calling birds',
      'Three french hens',
      'Two turtle doves and',
      'A partridge in a pear tree.',
    ]
    return stuffs.slice(stuffs.length - dayNumber).join('\n')
  }

  generateWholeSongForTheDay(dayNumber) {
    const firstLine = this.generateFirstLineForTheDay(dayNumber)
    const secondLine = this.generateSecondLine()
    const stuffs = this.generateTheStuffLineForTheDay(dayNumber)
    return [firstLine, secondLine, stuffs].join('\n')
  }
}
